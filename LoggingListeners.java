import com.google.common.util.concurrent.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.NumberFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Locale;
import java.util.concurrent.atomic.LongAdder;

class LoggingListeners implements Listeners {
 
    private static final Logger log = LoggerFactory.getLogger(LoggingListeners.class);
 
    private final Instant start = Instant.now();
    private final LongAdder solutionsFound = new LongAdder();
    private final LongAdder totalMoves = new LongAdder();
    private final RateLimiter logLimiter = RateLimiter.create(0.1);
    private boolean started = false;

    @Override
    public void onCreate(String reachability) {
        if (!started && logLimiter.tryAcquire()) {
            started = true;
            log.info("Possible moves per cell:{}",
                    reachability);
        }
    }

    @Override
    public void onSolution(String board, long moves) {
        solutionsFound.increment();
        if (logLimiter.tryAcquire()) {
            final long duration = Duration.between(start, Instant.now()).toSeconds();
            log.info("Found {} unique solutions in {} s ({}/s) or {} solutions counting mirrors and rotations{}",
                    longUSFormat(solutionsFound.longValue()),
                    longUSFormat(duration),
                    throughput(solutionsFound.longValue(), duration),
                    longUSFormat(solutionsFound.longValue() * 8),
                    board);
        }
    }

    @Override
    public void onEnd(long moves) {
        final Duration duration = Duration.between(start, Instant.now());
        totalMoves.add(moves);
        log.info("Overall speed: {} total moves in {} ({}K/s), found {} unique solutions ({}/s), {} solutions counting mirrors and rotations",
                longUSFormat(totalMoves.longValue()),
                duration,
                throughput(totalMoves.longValue(), duration.toMillis()),
                longUSFormat(solutionsFound.longValue()),
                throughput(solutionsFound.longValue() * 1000, duration.toMillis()),
                longUSFormat(solutionsFound.longValue() * 8));
    }
 
    private int throughput(long count, long duration) {
        return (int) (count / Math.max(duration, 1));
    }

    private String longUSFormat(long l) {
        return NumberFormat
                .getNumberInstance(Locale.US)
                .format(l);
    }
}
