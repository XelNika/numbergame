interface Listeners {
    void onCreate(String reachability);

    void onSolution(String board, long moves);

    void onEnd(long counter);
}
