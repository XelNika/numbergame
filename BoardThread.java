import java.util.concurrent.LinkedBlockingQueue;

public class BoardThread implements Runnable {
    private LinkedBlockingQueue<Board> queue;
    BoardThread(LinkedBlockingQueue<Board> q) {
        queue = q;
    }

    public void run() {
        while (!queue.isEmpty()) {
            Board b = null;
            try {
                b = queue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b != null) {
                b.explore();
            }
        }
    }
}
