import java.util.List;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

public class NumberGame {
    static int SIZE;
    static int HALF_SIZE_INDEX;
    static final private LinkedBlockingQueue<Board> startingBoards = new LinkedBlockingQueue<>();

    public static void main(String[] args) {
        System.out.println("Enter grid size:");
        Scanner s = new Scanner(System.in);
        SIZE = s.nextInt();
        HALF_SIZE_INDEX = (SIZE-1)/2;

        final Listeners listeners = new LoggingListeners();

        for (int x = 0; x <= HALF_SIZE_INDEX; x++) {
            for (int y = 0; y <= HALF_SIZE_INDEX; y++) {
                if (y > x) {
                    continue;
                }
                List<Board> l = Board.generateStartingBoards(x, y, listeners);

                for (Board b : l) {
                    try {
                        startingBoards.put(b);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        int threadCount = Runtime.getRuntime().availableProcessors();
        BoardThread[] threads = new BoardThread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            threads[i] = new BoardThread(startingBoards);
            new Thread(threads[i]).start();
        }
    }
}
