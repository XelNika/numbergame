import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Board {
    private static final int SIZE = NumberGame.SIZE;
    private final int MAX = SIZE * SIZE;

    private static final int HV_OFFSET = 3;
    private static final int DIAG_OFFSET = 2;

    private final int[][] possibleMoves = new int[MAX][];
    private final int[] reachableFrom = new int[MAX];
    private LinkedList<Integer> path = new LinkedList<>();
    private int startScore = 1;
    private int currentIndex;
    private int deadEnds = 0;

    private final Listeners listeners;
    private long counter = 0;

    Board(int x, int y, Listeners listeners) {
        int index = getIndex(x, y);
        currentIndex = getIndex(x,y);
        path.add(index);
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                List<Integer> moves = possibleMovesFrom(getIndex(i, j));
                int[] array = new int[moves.size()];
                int  k = 0;
                for (int m : moves) {
                    array[k++] = m;
                }
                possibleMoves[i+j*SIZE] = array;
            }
        }
        for (int k = 0; k < MAX; k++) {
            reachableFrom[k] = possibleMoves[k].length;
        }
        listeners.onCreate(prettyPrint(reachableFrom));
        reachableFrom[index] = -reachableFrom[index];
        this.listeners = listeners;
    }

    Board(Board b) {
        this.currentIndex = b.currentIndex;
        this.path.addAll(b.path);
        System.arraycopy(b.possibleMoves, 0, this.possibleMoves, 0, MAX);
        System.arraycopy(b.reachableFrom, 0, this.reachableFrom, 0, MAX);
        this.deadEnds = b.deadEnds;
        this.startScore = path.size();

        this.listeners = b.listeners;
        this.counter = b.counter;
    }


    private List<Integer> possibleMoves() {
        return possibleMovesFrom(currentIndex);
    }

    private List<Integer> possibleMovesFrom(int index) {
        List<Integer> moves = new ArrayList<>(8);

        for (Direction dir : Direction.values()) {
            int xNew = getX(index) + dir.dx;
            int yNew = getY(index) + dir.dy;
            if (withinBoard(xNew, yNew)) {
                moves.add(index + dir.dIndex);
            }
        }
        return moves;
    }

    private boolean withinBoard(int xNew, int yNew) {
        return (xNew >= 0 && xNew < SIZE
                && yNew >= 0 && yNew < SIZE);
    }

    void explore() {
        if (path.size() == MAX) {
            listeners.onSolution("\n" + toString(), counter);
        } else {
            for (Integer nextMove : possibleMoves[currentIndex]) {
                if (worthVisiting(nextMove)) {
                    move(nextMove);
                    explore();
                }
            }
        }
        undo();
    }

    private boolean worthVisiting(int index) {
        return reachableFrom[index] > 0 && deadEnds < 2;
    }

    private void move(int index) {
        reachableFrom[index] = -reachableFrom[index];
        for (int i : possibleMoves[currentIndex]) {
            int temp = reachableFrom[i];
            if (temp > 0) {
                reachableFrom[i]--;
                if (temp <= 2 && i != index) {
                    deadEnds++;
                }
            }
        }
        path.add(index);
        currentIndex = index;

        counter++;
    }

    private void undo() {
        if (path.size() > startScore) {
            int stuck = path.removeLast();
            int lastIndex = path.getLast();

            for (int i : possibleMoves[lastIndex]) {
                int temp = reachableFrom[i];
                if (temp >= 0) {
                    reachableFrom[i]++;
                    if (temp <= 1 && i != stuck) {
                        deadEnds--;
                    }
                }
            }
            reachableFrom[stuck] = -reachableFrom[stuck];
            currentIndex = lastIndex;
        } else {
            listeners.onEnd(counter);
        }
    }

    private int getIndex(int x, int y) {
        return y * SIZE + x;
    }

    private int getX(int index) {
        return index % SIZE;
    }

    private int getY(int index) {
        return index / SIZE;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(MAX*4);
        sb.append(path.getFirst());
        for (int i = 1; i < path.size(); i++) {
            sb.append("->");
            sb.append(path.get(i));
        }
        return sb.toString();
    }

    private int[] createGridArray() {
        int[] array = new int[path.size()];
        int score = 1;
        for (int i : path) {
            array [i] = score++;
        }
        return array;
    }

    private String prettyPrint(int[] a) {
        StringBuilder sb = new StringBuilder(MAX*4+SIZE);
        for (int y = 0; y < SIZE; ++y) {
            sb.append('\n');
            for (int x = 0; x < SIZE; ++x) {
                final int value = a[getIndex(x, y)];
                if (value < 1000) {
                    sb.append(' ');
                }
                if (value < 100) {
                    sb.append(' ');
                }
                if (value < 10) {
                    sb.append(' ');
                }
                sb.append(value);
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    static List<Board> generateStartingBoards(int x, int y, Listeners l) {
        Board b = new Board(x, y, l);
        List<Board> boards = new ArrayList<>();
        b.findUniqueStartingPoints(boards);
        return boards;
    }

    private void findUniqueStartingPoints(List<Board> list) {
        int[] moves = possibleMoves[currentIndex];
        for (Integer index : moves) {
            if (!worthVisiting(index)) {
                continue;
            }
            Board bCopy = new Board(this);
            bCopy.move(index);
            bCopy.startScore++;

            if (bCopy.startScore == 2 && bCopy.path.getFirst() == MAX / 2 && SIZE % 2 == 1) {
                moves = possibleMoves[index];
                for (Integer m : moves) {
                    if (worthVisiting(m)) {
                        bCopy.move(m);
                        bCopy.startScore++;
                        break;
                    }
                }
                list.add(bCopy);
                break;
            }

            if (bCopy.uniquePath()) {
                list.add(bCopy);
            } else if (bCopy.onMirror()) {
                bCopy.findUniqueStartingPoints(list);
            }
        }
    }

    private boolean uniquePath() {
        int indexStart = path.getFirst();
        int xStart = getX(indexStart);
        int yStart = getY(indexStart);

        int indexNew = path.getLast();
        int yNew = getY(indexNew);
        int xNew = getX(indexNew);

        boolean unique = (xStart != yStart);
        if (SIZE % 2 == 1) {
            unique &= xStart != NumberGame.HALF_SIZE_INDEX;
        }
        boolean mirrorReady = onMirror();

        if (!mirrorReady && (yNew > xNew)) {
            mirrorReady = true;
        } else if (SIZE % 2 == 1 && !mirrorReady && xNew < NumberGame.HALF_SIZE_INDEX) {
            mirrorReady = true;
        }

        return unique || !mirrorReady;
    }

    private boolean onMirror() {
        boolean onDiag = true;
        boolean onVert = true;

        if (SIZE % 2 == 0) {
            onVert = false;
        }

        for (int i : path) {
            int x = getX(i);
            int y = getY(i);
            if (x != y) {
                onDiag = false;
            }
            if (x != NumberGame.HALF_SIZE_INDEX) {
                onVert = false;
            }
        }
        return onDiag || onVert;
    }

    private enum Direction {
        DOWN(0, HV_OFFSET),
        DOWN_RIGHT(DIAG_OFFSET, DIAG_OFFSET),
        RIGHT(HV_OFFSET, 0),
        UP_RIGHT(DIAG_OFFSET, -DIAG_OFFSET),
        UP(0, -HV_OFFSET),
        UP_LEFT(-DIAG_OFFSET, -DIAG_OFFSET),
        LEFT(-HV_OFFSET, 0),
        DOWN_LEFT(-DIAG_OFFSET, DIAG_OFFSET);

        final int dx;
        final int dy;
        final int dIndex;

        Direction(int dx, int dy) {
            this.dx = dx;
            this.dy = dy;
            dIndex = dy * SIZE + dx;
        }
    }
}